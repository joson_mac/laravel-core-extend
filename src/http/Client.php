<?php
namespace Woson\LaravleCoreExtend\http;
use Illuminate\Support\Facades\Log;
class Client {
	public $userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 15_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6.1 Mobile/15E148 Safari/604.1";
	private $ispost = 0;//0、get;1、post 
	private $proxyServer  = "";
	private $headers = "";
	private $method = ""; //空则默认get,其它为Post，没有写put,delete
	private $https = 0;//忽略证书检查

	public function curl($url, $params = false,$json=1,$isProxy=1){

        $httpInfo = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if(!empty($this->headers)){
        	curl_setopt($ch, CURLOPT_HEADER, false);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        	// var_dump($this->headers);
        }
        
        if ($this->https) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 对认证证书来源的检查
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // 从证书中检查SSL加密算法是否存在
        }
        if ($this->method == "post") {
        	if(is_array($params)){
        		$params = json_encode($params);
        	}
        	// if(!empty($params)){
        	// 	var_dump($params);
        	// }
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params) {
                if (is_array($params)) {
                    $params = http_build_query($params);
                }
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }

        if(!empty($this->proxyServer) && $isProxy==1){
        	// 设置代理服务器
	        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, false);
       	    curl_setopt($ch, CURLOPT_PROXYTYPE, 0); //http

        	// curl_setopt($ch, CURLOPT_PROXYTYPE, 5); //sock5

        	curl_setopt($ch, CURLOPT_PROXY, $this->proxyServer);
        	// 设置隧道验证信息
        	curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
        }
        

        $response = curl_exec($ch);
        
        if ($response === FALSE) {
        	Log::info("url:".$url."curl Error:".curl_error($ch));
            return false;
        }
        Log::info("url:".$url."response :".$response);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);
        if($json == 1){
        	return json_decode($response);
        }else{
        	return $response;
        }
	}

	public function setUseragent($type ="android",$agent=""){
		if(!empty($agent)){
			$this->userAgent = $agent;
		}
		if($type == "iphone"){
			$this->userAgent ="Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2";
		}elseif ($type == "android") {
			$this->userAgent = 'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-cn; BLA-AL00 Build/HUAWEIBLA-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/8.9 Mobile Safari/537.36';
		}else{
			$this->userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36';
		}

	}
	/**
	 * 代理服务器$proxyServer = "http":"http://ip:port";
	 *
	 * @param      <type>  $ip     The new value
	 * @param      <type>  $port   The port
	 */
	public function setPoxy($proxyServer){
		$this->proxyServer = $proxyServer;
	}

	public function setHeaders($headers = []){
		$this->headers = $headers;
	}


	public function setMethod($action =""){
		$this->method = $action;
	}

	public function setHttps($is=0){
		$this->https = $is;
	}


}